import sys

articulos = {}


def anadir(articulo, precio):
    articulos[articulo] = precio


def mostrar():
    print("Lista de articulos:")
    for articulo, precio in articulos.items():
        print(f"{articulo}: {precio} euros")


def pedir_articulo():
    while True:
        articulo = input("¿Qué articulo quiere comprar? ")
        if articulo in articulos:
            return articulo
        print(f"No tenemos el articulo {articulo}")


def pedir_cantidad():
    while True:
        try:
            cantidad = int(input("¿Qué cantidad quiere comprar? "))
            if cantidad <= 0:
                print("La cantidad debe ser mayor que 0")
                continue
            return cantidad
        except ValueError:
            print("La cantidad debe ser un número entero")


def main():
    if len(sys.argv) < 2:
        print("No se han especificado articulos")
        return

        # Añadir articulos del argumento
    articulo_anterior = ""
    for i in range(1, len(sys.argv), 2):
        articulo = sys.argv[i]
        if i == len(sys.argv) - 1:
            print(f"Error en argumentos: Hay al menos un articulo sin precio: {articulo}")
            return
        try:
            precio = float(sys.argv[i + 1])
        except ValueError:
            print(f"Error en argumentos: {sys.argv[i + 1]} no es un precio correcto")
            return
        anadir(articulo, precio)
        articulo_anterior = articulo

    mostrar()

    articulo = pedir_articulo()
    cantidad = pedir_cantidad()

    print(f"Ha comprado {cantidad} unidades de {articulo}")
    print(f"Importe total: {cantidad * articulos[articulo]} euros")


if __name__ == "__main__":
    main()






